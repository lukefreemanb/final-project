﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevel : MonoBehaviour {

    public MouseLook MouseLook;
    public MouseLook MouseLookPlayer;
    public LockMouse LockMouse;
    public GameObject GameHUD;
    public GameObject EndScreenTimer;
    public PlayerMove PlayerMove;
    public GameObject EndScreenHUD;
    public LTimer EndLTimer;

    public Text EndScreenTimerText;


	// Use this for initialization
	void Start () {
        EndScreenHUD.SetActive(false);
        EndScreenTimerText.CrossFadeAlpha(0, 0, true);
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider col) {

        //MouseLook.enabled = false;
        MouseLookPlayer.enabled = false;
        LockMouse.enabled = false;
        GameHUD.SetActive(false);
        PlayerMove.enabled = false;
        EndLTimer.enabled = false;
        EndScreenHUD.SetActive(true);
        EndScreenTimerText.CrossFadeAlpha(100, 0, true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

	}

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Game restarted");
            SceneManager.LoadScene("l1");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Game quit");
            Application.Quit();
        }
    }


}
