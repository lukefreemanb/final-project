﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointControl : MonoBehaviour {

    public GameObject Player;
    public GameObject PlayerRB;
    private Vector3 cpPosition;
    public PlayerResetControl accessPRC;

	// Use this for initialization
	void Start () {
        accessPRC = Player.GetComponent<PlayerResetControl>();
        cpPosition = transform.position;
	}

    // Update is called once per frame
    void OnTriggerEnter(Collider col) {
        accessPRC.startingPosition = cpPosition;
        Debug.Log("checkpoint set");
	}

    void Update ()
    {
        //Debug.Log(accessPRC.startingPosition);
    }
}
