﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camJumpBob : MonoBehaviour {

    public float DownBobSpd = 0.1f;
    public float UpBobSpd = 0.02f;
    private Vector3 thisV3;
    private float startBob;
    private float lowBob;
    public float lowBobAmt = 1.0f;

	// Use this for initialization
	void Start () {
        startBob = thisV3.y;
        lowBob = thisV3.y - lowBobAmt;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Jump"))
        {
            thisV3.y = Mathf.Lerp(thisV3.y, lowBob, DownBobSpd);
        } else if (thisV3.y == lowBob) {
            thisV3.y = Mathf.Lerp(thisV3.y, startBob, UpBobSpd);
        }
	}
}
