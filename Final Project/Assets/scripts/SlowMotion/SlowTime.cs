﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTime : MonoBehaviour {

    public float maxBar = 100.0f;
    public float deplete = 0.1f;
    public float minBar = 0.0f;
    public float recharge = 0.5f;
    private float value;
    private bool canSlow;

	// Use this for initialization
	void Start () {
        value = maxBar;
        canSlow = true;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Value " + value);
        if (value <= minBar)
        {
            canSlow = false;
        } else
        {
            canSlow = true;
        }


        if (Input.GetButton("Fire1") && value > minBar)
        {
            value -= deplete;
        }
        else
        {
            if (value <= maxBar)
            {
                value += recharge;
            }

        }

        if (canSlow == true && Input.GetButtonDown("Fire1"))
        {
            if (Time.timeScale == 1.0F)
            {
                Time.timeScale = 0.2F;
            }
            else
            {
                Time.timeScale = 1.0F;
            }
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            Time.timeScale = 1.0f;
            Time.fixedDeltaTime = 0.015F * Time.timeScale;
        }
        else if (canSlow == false)
        {
            Time.timeScale = 1.0f;
            Time.fixedDeltaTime = 0.015F * Time.timeScale;
        }

    }
}

