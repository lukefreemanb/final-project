﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour {

    public float moveSpeed = 4.0f;
    public float runSpeed = 6.0f;

    public float jumpSpeed = 2.0f;
    public float floatSpeed = 0.5f;
    private float gravity;
    public float normalGravity = 5.0f;
    //public float fallGravity = 6.0f;
    public bool airControl = true;

    public float JumpCast = 5.0f;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private bool isGrounded = false;
    private bool hasGrounded = false;
    private Transform myTransform;
    private float speed;
    private bool inJumpRange = false;
    public float antiBumpFactor = 0.75f;

    // Use this for initialization
    void Start () {
        controller = GetComponent<CharacterController>();
        myTransform = transform;
        speed = moveSpeed;
	}

    // Update is called once per frame
    void Update () {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        //limitting diagonal speed
        float inputModifyFactor = (inputX != 0.0f && inputY != 0.0f) ? .7071f : 1.0f;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Game quit");
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Game Restarted");
            SceneManager.LoadScene("l1");
        }

        if (Input.GetButtonDown("Jump"))
        {
           // Debug.Log("isGrounded" + isGrounded);
            //Debug.Log("inJumpRange" + inJumpRange);
           // Debug.Log("hasGrounded" + hasGrounded);
        }

        if (Physics.Raycast(myTransform.position, -Vector3.up, JumpCast))
        {
            inJumpRange = true;
        }
        else
        {
            inJumpRange = false;
        }

        if (isGrounded)
        {

            moveDirection = new Vector3(inputX * inputModifyFactor, -antiBumpFactor, inputY * inputModifyFactor);
            moveDirection = myTransform.TransformDirection(moveDirection) * speed;

            speed = Input.GetButton("Run") ? runSpeed : moveSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;

            }
            hasGrounded = true;
            gravity = normalGravity;

        }
        else
        {

            if (Input.GetButtonUp("Jump"))
            {
                hasGrounded = false;
                //gravity = fallGravity;

            }
            if (airControl)
            {
                moveDirection.x = inputX * speed * inputModifyFactor;
                moveDirection.z = inputY * speed * inputModifyFactor;
                moveDirection = myTransform.TransformDirection(moveDirection);
            }

        }

        

        // set grounded
        isGrounded = (controller.Move(moveDirection * Time.deltaTime) & CollisionFlags.Below) != 0;
    }

    void FixedUpdate()
    {
        // gravity
        moveDirection.y -= gravity * Time.deltaTime;

        if (!isGrounded)
        {
            if (inJumpRange && hasGrounded && Input.GetButton("Jump"))
            {
                moveDirection.y += floatSpeed;
            }
        }
    }
}
