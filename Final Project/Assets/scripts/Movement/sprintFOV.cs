﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sprintFOV : MonoBehaviour {

    public float newFOV = 90.0f;
    public float normalFOV = 80.0f;
    public float zoomSpeed = 0.0001f;

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {
        //Camera.main.fieldOfView = Input.GetButton("Run") ? newFOV : normalFOV;

        if (Input.GetButton("Run")) {

            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, newFOV, zoomSpeed);

        } else
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, normalFOV, zoomSpeed);
        }

	}
}
