﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuControl : MonoBehaviour {

    public GameObject MainUI;
    public GameObject HelpUI;

    public void PlayButtonClick()
    {
        SceneManager.LoadScene("l1");
    }

    public void HelpButtonClick()
    {
        MainUI.SetActive(false);
        HelpUI.SetActive(true);
    }

    public void HelpBackButtonClick()
    {
        HelpUI.SetActive(false);
        MainUI.SetActive(true);
    }

    public void QuitButtonClick()
    {
        Application.Quit();
    }

}
