﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlowBar : MonoBehaviour {

    public Slider slider;

    public float maxBar = 100.0f;
    public float deplete = 0.1f;
    public float minBar = 0.0f;
    public float recharge = 0.5f;

    // Use this for initialization
    void Start () {
        slider.value = maxBar;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("SliderValue " + slider.value);
        if (Input.GetButton("Fire1") && slider.value > minBar - 1)
        {
            slider.value -= deplete;
        }
        else
        {
            if (slider.value <= maxBar)
            {
                slider.value += recharge;
            }

        }
    }
}
