﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LTimer : MonoBehaviour {

    public Text timerText;
    public float mSeconds, seconds, minutes;

    // Use this for initialization
    void Start () {
        timerText = GetComponent<Text>() as Text;
	}
	
	// Update is called once per frame
	void Update () {
        minutes = (int)(Time.timeSinceLevelLoad / 60f);
        seconds = (int)(Time.timeSinceLevelLoad % 60f);
        mSeconds = (int)(Time.timeSinceLevelLoad * 100f) % 100;

        timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00") + "." + mSeconds.ToString("00");
	}
}
