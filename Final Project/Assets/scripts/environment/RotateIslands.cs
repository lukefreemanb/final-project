﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateIslands : MonoBehaviour {

    public float ySpeed = 5.0f;
    public float xSpeed = 5.0f;
    public float zSpeed = 5.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate((Vector3.up * Time.deltaTime) * ySpeed);
        transform.Rotate((Vector3.right * Time.deltaTime) * xSpeed);
        transform.Rotate((Vector3.forward * Time.deltaTime) * zSpeed);
    }
}
